/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.fahp.model;

import com.ttm.fahp.model.InputKriteria;
import com.ttm.fahp.model.SaveCSV;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author teguhteja
 */
public class ModelCFAHP {

    /**
     * @return the column
     */
    public String[] getColumn() {
        return column;
    }

    /**
     * @return the data
     */
    public Object[][] getData() {
        return data;
    }
    
    String fileName;
    private String[] column;
    private Object[][] data;
//    private Object[][] kriteria;
    Hashtable dictKriteria;
    private int index;
    private float total;
    
    public ModelCFAHP(String fileName, Object[][] kriteria, int index){
        this.fileName = fileName;
        this.index = index;
        initDictKriteria(kriteria);
        prosesCompareFAHP();
    }

    private void prosesCompareFAHP() {
        BufferedReader br = null;
        try {
            String line = "";
            String splitBy = ";";
            LinkedList<String> content = new LinkedList<>();
            total = 0.0f;
            //parsing a CSV file into BufferedReader class constructor
            boolean isColumnName = true;
            br = new BufferedReader(new FileReader(fileName));
            while ((line = br.readLine()) != null) {
                if (isColumnName) {
                    isColumnName = false;
                    column = line.split(splitBy);
                } else {
                    content.add(line);
                }
            }
            data = new Object[content.size()][getColumn().length];
            for (int i = 0; i < content.size(); i++) {
                data[i] = content.get(i).split(splitBy);
            }
            
            for(int i=0; i < data.length;i++){
//                System.out.println(i);
                data[i][6] = skorA(data[i][2]);
                data[i][7] = skorM(data[i][3]);
                data[i][8] = skorJ(data[i][4]);
                data[i][9] = skorK(data[i][5]);
                data[i][10] = pengaliSkorUtama(data[i][6],"Absensi"); 
                data[i][11] = pengaliSkorUtama(data[i][7],"Masa Kerja"); 
                data[i][12] = pengaliSkorUtama(data[i][8],"Jabatan"); 
                data[i][13] = pengaliSkorUtama(data[i][9],"Kinerja");
                data[i][14] = sumTotal(data[i][10],data[i][11],data[i][12],data[i][13]);
                total = getTotal() + Float.valueOf(data[i][14].toString());
            }
            DecimalFormat df = new DecimalFormat("#,###.00"); 
            for(int i=0; i < data.length;i++){
                 data[i][15] = df.format(new BigDecimal((String) calcBonus(getTotal(), data[i][14]))); 
             }
            saveFileOutput();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(InputKriteria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(InputKriteria.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(InputKriteria.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void initDictKriteria(Object[][] data) {
        this.dictKriteria = new Hashtable<String, Float[]>();
        for(int i=0; i < data.length; i++){
            String key = String.valueOf(data[i][0]);
            Float[] values = new Float[]{
                Float.valueOf((String) data[i][1]),
                Float.valueOf((String) data[i][2])};
            dictKriteria.put(key, values);
        }
    }

    private Object skorA(Object object) {
        int value = Integer.valueOf((String) object);
        Object retVal = null;
        if(value < 14){
            Object[] dictVal = (Object[]) dictKriteria.get("<14");
            retVal = (Object)dictVal[index];
        }else{
            Object[] dictVal = (Object[]) dictKriteria.get(">14");
            retVal = (Object)dictVal[index];
        }
        return retVal.toString();
    }

    private Object skorM(Object object) {
        float value = Float.valueOf((String) object);
        Object retVal = null;
        if(value > 10){
            Object[] dictVal = (Object[]) dictKriteria.get(">10th");
            retVal = (Object)dictVal[index];
        }else if (value > 5 && value <= 10){
            Object[] dictVal = (Object[]) dictKriteria.get("5-10th");
            retVal = (Object)dictVal[index];
        }else if (value > 1 && value <= 5){
            Object[] dictVal = (Object[]) dictKriteria.get("1-5thn");
            retVal = (Object)dictVal[index];
        }else {
            Object[] dictVal = (Object[]) dictKriteria.get("<1thn");
            retVal = (Object)dictVal[index];
        }
        return retVal.toString();
    }

    private Object skorJ(Object object) {
        String value = (String) object;
        Object retVal = null;
        Object[] dictVal = (Object[]) dictKriteria.get(value);
        retVal = (Object)dictVal[index];
        return retVal.toString();    
    }

    private Object skorK(Object object) {
         int value = Integer.valueOf((String) object);
        Object retVal = null;
        if(value > 35 && value <= 40){
            Object[] dictVal = (Object[]) dictKriteria.get("36-40");
            retVal = (Object)dictVal[index];
        }else if (value > 25 && value <= 35){
            Object[] dictVal = (Object[]) dictKriteria.get("25-35");
            retVal = (Object)dictVal[index];
        }else {
            Object[] dictVal = (Object[]) dictKriteria.get("8-24");
            retVal = (Object)dictVal[index];
        }
        return retVal.toString();
    }

    private Object pengaliSkorUtama(Object nilai, String kriteria) {
        Object retVal = null;
        Object[] dictVal = (Object[]) dictKriteria.get(kriteria);
        float pengali = (float)dictVal[index];
        float fNilai = Float.valueOf(nilai.toString());
        retVal = fNilai * pengali;
        return retVal.toString();
    }

    private Object sumTotal(Object o1, Object o2, Object o3, Object o4) {
        Object retVal;
        float fN1 = Float.valueOf(o1.toString());
        float fN2 = Float.valueOf(o2.toString());
        float fN3 = Float.valueOf(o3.toString());
        float fN4 = Float.valueOf(o4.toString());
        retVal = fN1+fN2+fN3+fN4;
        return retVal.toString();
    }

    private Object calcBonus(float total, Object o1) {
       Object retVal = null;
       float pembilang = Float.valueOf(o1.toString());
       Object[] dictVal = (Object[]) dictKriteria.get("bonus");
       float totalBonus = (float)dictVal[index];
       retVal = pembilang/total*totalBonus;
       System.out.println(retVal+" : "+pembilang +"/"+ total+"*"+totalBonus);
       return retVal.toString();
    }

    private void saveFileOutput() throws IOException {
        String[] output = new String[]{"output/rahp.csv","output/rfahp.csv"};
        String saveFileName = output[index];
        FileWriter writer = new FileWriter(saveFileName);
        SaveCSV.writeLine(writer, column);
        for(int i=0; i < data.length; i++){
            String[] row = new String[column.length];
            for(int j=0; j < row.length;  j++){
                row[j] = data[i][j].toString();
            }
            SaveCSV.writeLine(writer, row);
        }
        writer.close();
        System.out.println("Save file "+saveFileName);
    }

    /**
     * @return the total
     */
    public float getTotal() {
        return total;
    }
    
    
}
