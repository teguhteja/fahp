/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.fahp.model;

import java.io.*;

public class ReadCSV {

    String filename;

    public ReadCSV(String filename) {
        this.filename = filename;
    }

    public BufferedReader getContent() throws FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader(filename));
        return br;
    }

//    public static void main(String[] args) throws Exception {
//        String line = "";
//        String splitBy = ";";
//        try {
////parsing a CSV file into BufferedReader class constructor  
//            BufferedReader br = new BufferedReader(new FileReader("data/fahp.csv"));
//            while ((line = br.readLine()) != null) //returns a Boolean value  
//            {
//                String[] arrLine = line.split(splitBy);    // use comma as separator  
////                System.out.println("Employee [First Name=" + employee[0] + ", Last Name=" + employee[1] + ", Designation=" + employee[2] + ", Contact=" + employee[3] + ", Salary= " + employee[4] + ", City= " + employee[5] + "]");
//                for (String s : arrLine) {
//                    System.out.print(s + " ");
//                }
//                System.out.println("");
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
