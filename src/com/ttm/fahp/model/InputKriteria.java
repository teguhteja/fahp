/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.fahp.model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author teguhteja
 */
public class InputKriteria {

    /**
     * @return the column
     */
    public String[] getColumn() {
        return column;
    }

    /**
     * @return the data
     */
    public Object[][] getData() {
        return data;
    }
    private String[] column;
    private Object[][] data;
    
    public InputKriteria(){
        initFAHP();
    }
    
    private void initFAHP() {
        String[] inputFile ={
           "output/ahpabsensi.csv", "output/ahpjabatan.csv","output/ahpkinerja.csv", 
            "output/ahpkriteria.csv","output/ahpmasakerja.csv", "output/fahpabsensi.csv",
            "output/fahpjabatan.csv", "output/fahpkinerja.csv","output/fahpkriteria.csv", 
            "output/fahpmasakerja.csv","output/ahpbonus.csv","output/fahpbonus.csv",
        };
        column = new String[]{"Kriteria","AHP","FAHP"};
        Hashtable<String, String> dictAhp = new Hashtable<String, String>();
        Hashtable<String, String> dictFahp = new Hashtable<String, String>();
        for(String file : inputFile){
            String[][] content = readFileCsv(file);
            if(file.startsWith("output/ahp")){
                for(int i=0; i<content[0].length;i++){
//                    System.out.println(content[0][i]+":"+content[1][i]);
                    dictAhp.put(content[0][i], content[1][i]);
                }
            }else{
                 for(int i=0; i<content[0].length;i++){
                    dictFahp.put(content[0][i], content[1][i]);
                }
            }
        }
        int index = 0;
        data = new Object[dictAhp.size()][3];
        for(Enumeration key = dictAhp.keys(); key.hasMoreElements();){
            Object oKey = key.nextElement();
            index = getIndexBasedKey(oKey);
            data[index][1] = dictAhp.get(oKey);
            data[index][2] = dictFahp.get(oKey);
            data[index][0] = oKey;
            
        }
    }
    
    private String[][] readFileCsv(String fileName) {
        BufferedReader br = null;
        String[][] retVal = null;
        try {
            String line = "";
            String splitBy = ";";
            String[] columnName;
            String[] content;
            //parsing a CSV file into BufferedReader class constructor
            boolean isColumnName = true;
            br = new BufferedReader(new FileReader(fileName));
            while ((line = br.readLine()) != null) {
                if (isColumnName) {
                    isColumnName = false;
                    columnName = line.split(splitBy);
                    retVal = new String[2][columnName.length];
                    retVal[0] = columnName;
                } else {
                    content = line.split(splitBy);
                    retVal[1] = content;
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(InputKriteria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(InputKriteria.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(InputKriteria.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retVal;
    }

    private int getIndexBasedKey(Object oKey) {
        int retVal=0;
        String sKey = oKey.toString();
        switch(sKey){
            case "Absensi":
                retVal = 0;
                break;
            case "<14":
                retVal = 1;
                break;
            case ">14":
                retVal = 2;
                break;
            case "Masa Kerja":
                retVal = 3;
                break;
            case ">10th":
                retVal = 4;
                break;
            case "5-10th":
                retVal = 5;
                break;
            case "1-5thn":
                retVal = 6;
                break;
           case "<1thn":
                retVal = 7;
                break;
            case "Jabatan":
                retVal = 8;
                break;
            case "Manager":
                retVal = 9;
                break;
            case "Supervisor":
                retVal = 10;
                break;
            case "Staff":
                retVal = 11;
                break;
            case "Kinerja":
                retVal = 12;
                break;
            case "36-40":
                retVal = 13;
                break;
            case "25-35":
                retVal = 14;
                break;
            case "8-24":
                retVal = 15;
                break;
            case "bonus":
                retVal = 16;
                break;
            default:
                retVal = 0;
                break;
        }
        return retVal;
    }
}
