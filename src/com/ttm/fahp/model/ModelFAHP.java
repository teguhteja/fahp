/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.fahp.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author teguhteja
 */
public class ModelFAHP {

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @return the mPbP
     */
    public float[][] getmPbP() {
        return mPbP;
    }

    /**
     * @return the panjang
     */
    public int getPanjang() {
        return panjang;
    }

    /**
     * @return the totKolom
     */
    public float[] getTotKolom() {
        return totKolom;
    }

    /**
     * @return the totBaris
     */
    public float[] getTotBaris() {
        return totBaris;
    }

    /**
     * @return the vecPriot
     */
    public float[] getVecPriot() {
        return vecPriot;
    }

    /**
     * @return the mCAHP02
     */
    public float[][] getmCAHP02() {
        return mCAHP02;
    }

    /**
     * @return the ratKons
     */
    public float[] getRatKons() {
        return ratKons;
    }

    /**
     * @return the bobPrio
     */
    public float[] getBobPrio() {
        return bobPrio;
    }

    /**
     * @return the lamMax
     */
    public float getLamMax() {
        return lamMax;
    }

    /**
     * @return the ci
     */
    public float getCi() {
        return ci;
    }

    /**
     * @return the cr
     */
    public float getCr() {
        return cr;
    }

    /**
     * @return the fse
     */
    public float[][] getFse() {
        return fse;
    }

    /**
     * @return the cFse
     */
    public float[][] getcFse() {
        return cFse;
    }

    /**
     * @return the minFse
     */
    public float[] getMinFse() {
        return minFse;
    }

    /**
     * @return the sumFse
     */
    public float getSumFse() {
        return sumFse;
    }

    /**
     * @return the bobFse
     */
    public float[] getBobFse() {
        return bobFse;
    }
    private String filename;
    private String[] columnName;
    private Object[][] data;
    private float[][] mCAHP;
    private float[][] mPbP;
    private int panjang;
    private float[] totKolom;
    private float[] totBaris;
    private float[] vecPriot;
    private float[][] mCAHP02;
    private float[] ratKons;
    private float[] bobPrio;
    private float lamMax;
    private float ci;
    private float cr;
    private float[][] fse;
    private float[][] cFse;
    private float[] minFse;
    private float sumFse;
    private float[] bobFse;
    private float[][] mPbF01;
    private float[] sMPbF01;
    private boolean isFAHP;
    private float[] prioSub;
    private String mode;

    public ModelFAHP(String filename, boolean isFAHP) {
        this.filename = filename;
        this.isFAHP = isFAHP;
//        this.panjang = getPBasedCol();
//        initMCAHP(panjang);
    }

    public ModelFAHP(String filename, int p) {
        this.filename = filename;
        this.panjang = p;
        initMCAHP(p);
    }

    /**
     * @return the columnName
     */
    public String[] getColumnName() {
        return columnName;
    }

    /**
     * @return the data
     */
    public Object[][] getData() {
        return data;
    }

    private float pembulatan(float f) {
        float retVal = 0.0f;
        retVal = (float) ((f % 1) >= 0.5 ? Math.ceil(f) : Math.floor(f));
        return retVal;
    }

    @SuppressWarnings("empty-statement")
    public void readCsv() {
        String line = "";
        String splitBy = ";";
        float[] mean;
        float[] pembulatan;
        LinkedList<String> content = new LinkedList<>();
        try {
            //parsing a CSV file into BufferedReader class constructor  
            boolean isColumnName = true;
            BufferedReader br = new BufferedReader(new FileReader(this.getFilename()));

            while ((line = br.readLine()) != null) {
                if (isColumnName) {
                    isColumnName = false;
                    columnName = line.split(splitBy);
                } else {
                    content.add(line);
                }
            }

            this.panjang = getPBasedCol();
            initMCAHP(getPanjang());

            data = new Object[content.size() + 2][getColumnName().length];
            for (int i = 0; i < content.size(); i++) {
                data[i] = content.get(i).split(splitBy);
            }

            mean = new float[getColumnName().length - 1];
            for (int j = 0; j < content.size(); j++) {
                for (int i = 1; i < getColumnName().length; i++) {
                    mean[i - 1] = mean[i - 1] + Float.valueOf((String) getData()[j][i]);
                }
            }
            data[content.size()][0] = "Rerata";
            data[content.size() + 1][0] = "Pembulatan";
            pembulatan = new float[getColumnName().length - 1];
            float temp = 0.0f;
            for (int i = 1; i < getColumnName().length; i++) {
                data[content.size()][i] = mean[i - 1] / content.size();
                temp = pembulatan(mean[i - 1] / content.size());
                data[content.size() + 1][i] = temp;
                pembulatan[i - 1] = temp;
            }
            prosesMCAHP(pembulatan);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initMCAHP(int p) {
        mCAHP = new float[p][p];
        for (int i = 0; i < p; i++) {
            mCAHP[i][i] = 1.0f;
        }
    }

    private void prosesMCAHP(float[] pembulatan) {
        int index = 0;
        totKolom = new float[getPanjang()];
        totBaris = new float[getPanjang()];
        vecPriot = new float[getPanjang()];
        mCAHP02 = new float[getPanjang()][getPanjang()];
        ratKons = new float[getPanjang()];
        bobPrio = new float[getPanjang()];
        lamMax = 0.0f;
        ci = 0.0f;
        cr = 0.0f;
        fse = new float[getPanjang()][3];
        cFse = new float[getPanjang()][getPanjang()];
        minFse = new float[getPanjang()];
        sumFse = 0.0f;
        bobFse = new float[getPanjang()];
        mPbF01 = new float[getPanjang()][3];
        sMPbF01 = new float[3];
        prioSub = new float[getPanjang()];

        // Matriks Perbandingan Berpasangan
        for (int i = 0; i < getPanjang(); i++) {
            for (int j = i + 1; j < getPanjang(); j++) {
                mCAHP[i][j] = pembulatan[index];
                mCAHP[j][i] = 1 / pembulatan[index];
                index++;
            }
        }

        for (int i = 0; i < getPanjang(); i++) {
            for (int j = 0; j < getPanjang(); j++) {
                totKolom[i] = getTotKolom()[i] + getmCAHP()[j][i];
            }
        }

        for (int i = 0; i < getPanjang(); i++) {
            for (int j = 0; j < getPanjang(); j++) {
                mCAHP02[j][i] = getmCAHP()[j][i] / getTotKolom()[i];
            }
        }
        // total baris dan vector prioritas
        float maxPrioSub = Float.NEGATIVE_INFINITY;
        for (int i = 0; i < getPanjang(); i++) {
            for (int j = 0; j < getPanjang(); j++) {
                totBaris[i] = getTotBaris()[i] + getmCAHP02()[i][j];
            }
            vecPriot[i] = getTotBaris()[i] / getPanjang();
            maxPrioSub = maxPrioSub < vecPriot[i] ? vecPriot[i] : maxPrioSub;   
        }

        if (isFAHP) {
            // Rasio Konsistensi 
            for (int i = 0; i < getPanjang(); i++) {
                float temp = 0.0f;
                for (int j = 0; j < getPanjang(); j++) {
                    ratKons[i] = getRatKons()[i] + getmCAHP()[i][j] * getVecPriot()[j];
                }
            }

            for (int i = 0; i < getPanjang(); i++) {
                bobPrio[i] = getRatKons()[i] / getVecPriot()[i];
                lamMax = getLamMax() + getBobPrio()[i];
            }
            lamMax = getLamMax() / 4;
            ci = (getLamMax() - getPanjang()) / (getPanjang() - 1);
            cr = getCi() / 0.9f;
            saveToCsv(bobPrio,"ahp");
        } else{
            // Matriks Nilai kriteria untuk sub kriteria
            for(int i=0 ; i < getPanjang(); i++){
//                prioSub[i] = vecPriot[i]/maxPrioSub;
                  prioSub[i] = vecPriot[i];
            }
            saveToCsv(prioSub,"ahp");
        }

        // Matriks Perbandingan berpasangan fuzzy
        initMPbF();

        for (int i = 0; i < getPanjang(); i++) {
            for (int j = 0; j < getPanjang(); j++) {
                mPbF01[i][0] = getmPbF01()[i][0] + getmPbP()[i][3 * j + 0];
                mPbF01[i][1] = getmPbF01()[i][1] + getmPbP()[i][3 * j + 1];
                mPbF01[i][2] = getmPbF01()[i][2] + getmPbP()[i][3 * j + 2];
            }
            sMPbF01[0] = getsMPbF01()[0] + getmPbF01()[i][0];
            sMPbF01[1] = getsMPbF01()[1] + getmPbF01()[i][1];
            sMPbF01[2] = getsMPbF01()[2] + getmPbF01()[i][2];
        }
        // Fuzzy Synthethic extent\

        for (int i = 0; i < getPanjang(); i++) {
            fse[i][0] = 1 / getsMPbF01()[2] * getmPbF01()[i][0];
            fse[i][1] = 1 / getsMPbF01()[1] * getmPbF01()[i][1];
            fse[i][2] = 1 / getsMPbF01()[0] * getmPbF01()[i][2];
        }
        minFse = replaceZero(getMinFse());
        for (int i = 0; i < getPanjang(); i++) {
            for (int j = 0; j < getPanjang(); j++) {
                if (i == j) {
                    cFse[i][j] = Float.NaN;
                } else {
                    cFse[i][j] = calcFse(getFse()[i][0], getFse()[i][1], getFse()[j][1], getFse()[j][2]);
                    minFse[j] = getMinFse()[j] > getcFse()[i][j] ? getcFse()[i][j] : getMinFse()[j];
                }
            }
        }
        for (int i = 0; i < getPanjang(); i++) {
            sumFse = getSumFse() + getMinFse()[i];
        }

        for (int i = 0; i < getPanjang(); i++) {
            bobFse[i] = getMinFse()[i] / getSumFse();
        }
        saveToCsv(bobFse,"fahp");
        
        System.out.println("Total Kolom :" + Arrays.toString(getTotKolom()));
        System.out.println("Total Baris : " + Arrays.toString(getTotBaris()));
        System.out.println("Matrix Prioritas : " + Arrays.deepToString(getmCAHP02()));
        System.out.println("Vector Prioritas : " + Arrays.toString(getVecPriot()));
        if (this.isFAHP) {
            System.out.println("Ratio Konsistensi : " + Arrays.toString(getRatKons()));
            System.out.println("Bobot Prioritas : " + Arrays.toString(getBobPrio()));
            System.out.println("Lamda Max : " + getLamMax());
            System.out.println("Concistency Index : " + getCi());
            System.out.println("Concistency Ratio : " + getCr());
        }else{
            System.out.println("Prioritas Subkriteria : " + Arrays.toString(getPrioSub()));
        }
        System.out.println("Matrix Fuzzy : " + Arrays.deepToString(getmPbP()));
        System.out.println("Total M. Fuzzy : " + Arrays.deepToString(getmPbF01()));
        System.out.println("Sum M. Fuzzy : " + Arrays.toString(getsMPbF01()));
        System.out.println("FSE : " + Arrays.deepToString(getFse()));
        System.out.println("Calc FSE : " + Arrays.deepToString(getcFse()));
        System.out.println("Min FSE : " + Arrays.toString(getMinFse()));
        System.out.println("Sum FSE : " + getSumFse());
        System.out.println("Bobot FSE : " + Arrays.toString(getBobFse()));
    }

    /**
     * @return the mCAHP
     */
    public float[][] getmCAHP() {
        return mCAHP;
    }

    private void initMPbF() {
        mPbP = new float[getPanjang()][getPanjang() * 3];
        for (int i = 0; i < getPanjang(); i++) {
            for (int j = 0; j < getPanjang(); j++) {
                if (i <= j) {
                    float[] sppb = tfn(getmCAHP()[i][j]);
                    for (int k = 0; k < 3; k++) {
                        mPbP[i][j * 3 + k] = sppb[k];
                    }
                } else {
                    float[] ksppb = ktfn(getmCAHP()[j][i]);
                    for (int k = 0; k < 3; k++) {
                        mPbP[i][j * 3 + k] = ksppb[k];
                    }
                }
            }
        }
    }

    private float[] tfn(float f) {
        int tingkatKepentingan = (int) f;
        float[] retVal = new float[]{0.0f, 0.0f, 0.0f};
        switch (tingkatKepentingan) {
            case 1:
                retVal = new float[]{1.0f, 1.0f, 1.0f};
                break;
            case 2:
                retVal = new float[]{1f / 2f, 1.0f, 3f / 2f};
                break;
            case 3:
                retVal = new float[]{1.0f, 3f / 2f, 2.0f};
                break;
            case 4:
                retVal = new float[]{3f / 2f, 2.0f, 5f / 2f};
                break;
            case 5:
                retVal = new float[]{2.0f, 5f / 2f, 3.0f};
                break;
            case 6:
                retVal = new float[]{5f / 2f, 3.0f, 7f / 2f};
                break;
            case 7:
                retVal = new float[]{3.0f, 7f / 2f, 4.0f};
                break;
            case 8:
                retVal = new float[]{7f / 2f, 4.0f, 9f / 2f};
                break;
            case 9:
                retVal = new float[]{4f, 9f / 2f, 9f / 2f};
                break;
            default:
                retVal = new float[]{0.0f, 0.0f, 0.0f};
                break;
        }
        return retVal;
    }

    private float[] ktfn(float f) {
        int tingkatKepentingan = (int) f;
        float[] retVal = new float[]{0.0f, 0.0f, 0.0f};
        switch (tingkatKepentingan) {
            case 1:
                retVal = new float[]{1.0f, 1.0f, 1.0f};
                break;
            case 2:
                retVal = new float[]{2f / 3f, 1.0f, 2f};
                break;
            case 3:
                retVal = new float[]{1f / 2f, 2f / 3f, 1.0f};
                break;
            case 4:
                retVal = new float[]{2f / 5f, 1f / 2f, 2f / 3f};
                break;
            case 5:
                retVal = new float[]{1f / 3f, 2f / 5f, 1f / 2f};
                break;
            case 6:
                retVal = new float[]{2f / 7f, 1f / 3f, 2f / 5f};
                break;
            case 7:
                retVal = new float[]{1f / 4f, 2f / 7f, 1f / 3f};
                break;
            case 8:
                retVal = new float[]{2f / 9f, 1f / 4f, 2f / 7f};
                break;
            case 9:
                retVal = new float[]{2f / 9f, 2f / 9f, 1f / 4f};
                break;
            default:
                retVal = new float[]{0.0f, 0.0f, 0.0f};
                break;

        }
        return retVal;
    }

    private float calcFse(float a, float b, float c, float d) {
//        System.out.println(a+"."+b+"."+c+"."+d);
        float retVal = 0.0f;
        if (c > b) {
            retVal = 1.0f;
        } else {
            if (a > d) {
                retVal = 0.0f;
            } else {
                retVal = (a - d) / ((c - d) - (b - a));
            }
        }
        return retVal;
    }

    private float[] replaceZero(float[] f) {
        float[] retVal = new float[f.length];
        for (int i = 0; i < f.length; i++) {
            retVal[i] = Float.MAX_VALUE;
        }
        return retVal;
    }

    private int getPBasedCol() {
        int retVal = 0;
        int pCol = getColumnName().length;
        switch (pCol) {
            case 2:
                retVal = 2;
                break;
            case 4:
                retVal = 3;
                break;
            case 7:
                retVal = 4;
                break;
            case 11:
                retVal = 5;
                break;
        }
        return retVal;
    }

    /**
     * @return the mPbF01
     */
    public float[][] getmPbF01() {
        return mPbF01;
    }

    /**
     * @return the sMPbF01
     */
    public float[] getsMPbF01() {
        return sMPbF01;
    }

    /**
     * @return the prioSub
     */
    public float[] getPrioSub() {
        return prioSub;
    }

    /**
     * @param mode the mode to set
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    private void saveToCsv(float[] row, String tipe) {
        FileWriter writer = null;
        String[] sRow = convertToString(row);
        String[] header = getHeader();
        String saveFileName = getSaveFileName().replaceAll("_", tipe);
        try {    
            writer = new FileWriter(saveFileName);
            SaveCSV.writeLine(writer, header);
            SaveCSV.writeLine(writer, sRow);
        } catch (IOException ex) {
            Logger.getLogger(ModelFAHP.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(ModelFAHP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private String[] getHeader() {
        String[] retVal = null;
        switch(this.mode){
            case "kriteria":
                retVal = new String[]{"Absensi", "Masa Kerja","Jabatan","Kinerja"};
                break;
            case "absensi":
                retVal = new String[]{"<14", ">14"};
                break;
            case "masakerja":
                retVal = new String[]{">10th", "5-10th","1-5thn","<1thn"};
                break;
            case "jabatan":
                retVal = new String[]{"Manager", "Supervisor","Staff"};
                break;
            case "kinerja":
                retVal = new String[]{"36-40", "25-35","8-24"};
                break;
            default:
                break;
                
        }
        return retVal;
    }

    private String getSaveFileName() {
        String retVal = null;
        switch(this.mode){
            case "kriteria":
                retVal = "output/_kriteria.csv";
                break;
            case "fahp":
                retVal = "output/_fahp.csv";
                break;
            case "absensi":
                retVal = "output/_absensi.csv";
                break;
            case "masakerja":
                retVal = "output/_masakerja.csv";
                break;
            case "jabatan":
                retVal = "output/_jabatan.csv";
                break;
            case "kinerja":
                retVal = "output/_kinerja.csv";
                break;
            default:
                break;
                
        }
        return retVal;
    }

    private String[] convertToString(float[] row) {
        String[] retVal = new String[row.length];
        for(int i=0; i<row.length;i++){
            retVal[i] = String.valueOf(row[i]);
        }
        return retVal;
    }
}
